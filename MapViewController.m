//
//  MapViewController.m
//  Teen Health Resource App
//
//  Created by Darryl Johnson on 4/9/14.
//  Copyright (c) 2014 Global App Initiative. All rights reserved.
//

#import "MapViewController.h"
#import "pheDatabase.h"
#import <Parse/Parse.h>

@interface MapViewController ()

@end

@implementation MapViewController

@synthesize MAP = _MAP;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
 
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    NSArray * points = [[PHEDatabase database] geoPointsforcity:[defaults objectForKey:@"CurrentCity"]];
    NSArray * names = [[PHEDatabase database] clinicsNamesWhereCityis:[defaults objectForKey:@"CurrentCity"]];
    NSArray * address = [[PHEDatabase database] clinicsAddressWhereCityis:[defaults objectForKey:@"CurrentCity"]];
    NSMutableArray * annotations = [[NSMutableArray alloc] initWithCapacity:100];
    
    
    
    
    
    int i = 0;
    for (NSString * point in points)
    {
    
        
        NSArray * array = [point componentsSeparatedByString:@","];
        
        
    CLLocationCoordinate2D coord = {([array[0] doubleValue]),([array[1] doubleValue])};
    
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    point.coordinate = coord;
        
        point.title = names[i];
        point.subtitle = address[i];
        
    
    
        

        [annotations addObject:point];
    
        i++;
    }
    [self.MAP showAnnotations:[NSArray arrayWithArray:annotations] animated:YES];
 
    
    
    
}



-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    

    
   
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/




@end
