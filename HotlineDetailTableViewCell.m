//
//  HotlineDetailTableViewCell.m
//  Teen Health Resource App
//
//  Created by Tom Strissel on 4/17/14.
//  Copyright (c) 2014 Global App Initiative. All rights reserved.
//

#import "HotlineDetailTableViewCell.h"
#import "HotlinesDetailViewController.h"

@implementation HotlineDetailTableViewCell
extern NSArray * phoneNumbers;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}
- (IBAction)callPhone:(id)sender {
    
    NSInteger * tag = ((UIButton * )sender).tag;
    
    
    NSString *number = [sender currentTitle];
    NSLog(@"%@", number);
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://1%@", phoneNumbers[(int)tag][1]]]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
