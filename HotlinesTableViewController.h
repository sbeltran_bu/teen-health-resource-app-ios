//
//  HotlinesTableViewController.h
//  Teen Health Resource App
//
//  Created by Tom Strissel on 3/19/14.
//  Copyright (c) 2014 Global App Initiative. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HotlinesDetailViewController;

@interface HotlinesTableViewController : UITableViewController <UITableViewDelegate, UITableViewDataSource>{
    NSArray *_hotlineCategoryInfos;
    HotlinesDetailViewController *_details;

}

@property (nonatomic, strong) NSArray *hotlineCategoryInfos;
@property (nonatomic, retain) HotlinesDetailViewController *details;

@end
