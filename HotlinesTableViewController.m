//
//  HotlinesTableViewController.m
//  Teen Health Resource App
//
//  Created by Tom Strissel on 3/19/14.
//  Copyright (c) 2014 Global App Initiative. All rights reserved.
//

#import "HotlinesTableViewController.h"
#import "pheDatabase.h"
#import "HotlineInfo.h"
#import "HotlinesDetailViewController.h"

@interface HotlinesTableViewController ()

@end

@implementation HotlinesTableViewController
@synthesize hotlineCategoryInfos = _hotlineCategoryInfos;
@synthesize details = _details;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    self.navigationItem.title = @"Hotlines"; //[defaults objectForKey:@"CurrentCityTitle"];
    self.hotlineCategoryInfos = [[PHEDatabase database] hotlineCategoryInfos];
   //[self.tableView setContentInset:UIEdgeInsetsMake(50,0,0,0)];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.hotlineCategoryInfos count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * identifer = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifer];
    if(cell == nil){
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifer];
    }
    
    // Configure the cell...
    HotlineInfo *info = [self.hotlineCategoryInfos objectAtIndex:indexPath.row];
    cell.textLabel.text = info.name;
    UIFont *myFont = [ UIFont fontWithName: @"Helvetica" size: 13.0 ];
    cell.textLabel.font  = myFont;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [self performSegueWithIdentifier:@"showDetails" sender:self];
}

/*
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.details == nil) {
    //self.details = [[HotlinesDetailViewController alloc] initWithNibName:@"HotlinesDetailViewController" bundle:nil];
    //UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"HotlinesDetailViewController"];
    UITableViewController *vc1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Hotlinesalksdjf"];
        
    //HotlineInfo *info = [_hotlineCategoryInfos objectAtIndex:indexPath.row];
    //details.uniqueId = info.uniqueId;
    [self.navigationController pushViewController:vc1 animated:YES];
    }
}
*/


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showDetails"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        HotlinesDetailViewController *destViewController = segue.destinationViewController;
        HotlineInfo *temp = [self.hotlineCategoryInfos objectAtIndex:indexPath.row];
        destViewController.hotlineId = temp.hotlineId;
}
}

@end
