//
//  HotlineDetailTableViewCell.h
//  Teen Health Resource App
//
//  Created by Tom Strissel on 4/17/14.
//  Copyright (c) 2014 Global App Initiative. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HotlineDetailTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UIButton *phone;
@property (weak, nonatomic) IBOutlet UILabel *extraDetails;

@end


