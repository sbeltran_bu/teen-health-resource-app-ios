//
//  ClinicsTableViewController.m
//  Teen Health Resource App
//
//  Created by Darryl Johnson on 4/14/14.
//  Copyright (c) 2014 Global App Initiative. All rights reserved.
//

#import "ClinicsTableViewController.h"
#import "ClinicsTableViewCell.h"
#import "pheDatabase.h"
@interface ClinicsTableViewController ()

@end

@implementation ClinicsTableViewController
NSMutableArray * clinic;
NSMutableArray * clinicProperties;
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    
    
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    self.navigationItem.title = [defaults objectForKey:@"CurrentCityTitle"];
    
    clinicProperties = [[NSMutableArray alloc] initWithArray:[[PHEDatabase database] clinicsiconsWhereCityNameIs:[defaults objectForKey:@"CurrentCity"]]];
    clinic = [[NSMutableArray alloc] initWithArray:[[PHEDatabase database] clinicsNamesWhereCityis:[defaults objectForKey:@"CurrentCity"]]];
    
    

    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
  
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    return [clinic count];
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    NSArray * clinics = [[PHEDatabase database] clinicsWhereCityIDis:[defaults objectForKey:@"CurrentCity"]];
    
    
    
    [defaults setObject:clinics[indexPath.item*5] forKey:@"CurrentClinic"];
    
    [self performSegueWithIdentifier:@"ClinicDetails" sender:self];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ClinicsCell" forIndexPath:indexPath];
    NSMutableArray * arrayOfImages = [[NSMutableArray alloc] initWithCapacity:20];

    ((ClinicsTableViewCell * )cell).image1.image = nil;
    ((ClinicsTableViewCell * )cell).image2.image = nil;
    ((ClinicsTableViewCell * )cell).image3.image = nil;
    
 
    if ([clinicProperties[indexPath.item*3] intValue] == 1)
    {
        [arrayOfImages addObject:@"lock.png"];
      
    }
    
    if ([clinicProperties[indexPath.item*3+1] intValue] == 0)
    {
         [arrayOfImages addObject:@"bigDollar.png"];
        
    }
    else
    {
        [arrayOfImages addObject:@"dollar.png"];
    }
    
    if ([clinicProperties[indexPath.item*3+2] intValue] == 1)
    {
       [arrayOfImages addObject:@"baby.png"];
        
    }
    
    @try {
        ((ClinicsTableViewCell *)cell).image1.image = [UIImage imageNamed:arrayOfImages[0]];
    }
    @catch (NSException *exception) {
        ((ClinicsTableViewCell *)cell).image1.image = [UIImage imageNamed:@""];
    }
    
    @try {
        ((ClinicsTableViewCell *)cell).image2.image = [UIImage imageNamed:arrayOfImages[1]];
    }
    @catch (NSException *exception) {
        ((ClinicsTableViewCell *)cell).image2.image = [UIImage imageNamed:@""];
    }
    
    @try {
        ((ClinicsTableViewCell *)cell).image3.image = [UIImage imageNamed:arrayOfImages[2]];
    }
    @catch (NSException *exception) {
        ((ClinicsTableViewCell *)cell).image3.image = [UIImage imageNamed:@""];
    }
    
   
    
    
    
    ((ClinicsTableViewCell *)cell).Title.text = clinic[indexPath.item];
    
    // Configure the cell...s
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)cancelButtonPressed:(id)sender {
    [self performSegueWithIdentifier:@"backToCity" sender:self];
}

@end
