//
//  HotlineInfo.m
//  Teen Health Resource App
//
//  Created by Tom Strissel on 4/5/14.
//  Copyright (c) 2014 Global App Initiative. All rights reserved.
//

#import "HotlineInfo.h"

@implementation HotlineInfo

@synthesize hotlineId = _hotlineId;
@synthesize name = _name;

- (id)init:(NSString*)hotlineId name:(NSString *)name {
    if ((self = [super init])) {
        self.hotlineId = hotlineId;
        self.name = name;
    }
    return self;
}

- (void) dealloc {
    self.name = nil;
    self.hotlineId = nil;
}

@end
