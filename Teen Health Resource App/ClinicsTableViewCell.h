//
//  ClinicsTableViewCell.h
//  Teen Health Resource App
//
//  Created by Darryl Johnson on 4/14/14.
//  Copyright (c) 2014 Global App Initiative. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClinicsTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *Title;
@property (strong, nonatomic) IBOutlet UIImageView *image1;
@property (strong, nonatomic) IBOutlet UIImageView *image2;
@property (strong, nonatomic) IBOutlet UIImageView *image3;

@end
