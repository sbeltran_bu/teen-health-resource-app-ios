//
//  HotlinesDetailViewController.m
//  Teen Health Resource App
//
//  Created by Tom Strissel on 4/14/14.
//  Copyright (c) 2014 Global App Initiative. All rights reserved.
//

#import "HotlinesDetailViewController.h"
#import "HotlinesTableViewController.h"
#import "PHEDatabase.h"
#import "HotlineDetailTableViewCell.h"


@interface HotlinesDetailViewController ()

@end

@implementation HotlinesDetailViewController

@synthesize hotlineId;
@synthesize details;
NSArray * phoneNumbers;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //NSLog(@"%@", details);
    
}

- (void)viewWillAppear:(BOOL)animated {
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    
    
    details = [[NSMutableArray alloc] initWithCapacity:200];
    details = [[PHEDatabase database] hotlineDetails:hotlineId cityId: [defaults objectForKey:@"CurrentCity"]];
    phoneNumbers = details;
    if (details.count == 0) {
        NSLog(@"Empty!");
        UIAlertView *notPermitted = [[UIAlertView alloc]
                                     initWithTitle:@"Oops!"
                                     message:@"Looks like there aren't any hotlies for your chosen city."
                                     delegate:nil
                                     cancelButtonTitle:@"OK"
                                     otherButtonTitles:nil];
        [notPermitted show];
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [details count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * identifer = @"hotlineTVC";
    
    HotlineDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifer];
    
    [cell.name setText:details[indexPath.item][0]];
    [cell.extraDetails setText:details[indexPath.item][2]];
    [cell.phone setTag:indexPath.item];
    return cell;
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/




@end
