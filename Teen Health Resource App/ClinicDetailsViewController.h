//
//  ClinicDetailsViewController.h
//  Teen Health Resource App
//
//  Created by Darryl Johnson on 4/15/14.
//  Copyright (c) 2014 Global App Initiative. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
@interface ClinicDetailsViewController : UIViewController <MKMapViewDelegate>
@property (strong, nonatomic) IBOutlet UIButton *directionsButton;
@property (strong, nonatomic) IBOutlet MKMapView *MAP;

@property (strong, nonatomic) IBOutlet UIButton *callButton;
@property (strong, nonatomic) IBOutlet UILabel *walkInYes;
@property (strong, nonatomic) IBOutlet UIImageView *img1;
@property (strong, nonatomic) IBOutlet UIImageView *img2;
@property (strong, nonatomic) IBOutlet UIImageView *img3;
@property (strong, nonatomic) IBOutlet UILabel *AddressLabel;
@property (strong, nonatomic) IBOutlet UITextView *hoursText;
@property (strong, nonatomic) IBOutlet UIView *viewToBeShadowed;
@property (strong, nonatomic) IBOutlet UILabel *clinicNameLabel;
@end
