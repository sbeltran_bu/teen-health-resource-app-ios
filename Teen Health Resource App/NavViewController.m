//
//  NavViewController.m
//  Teen Health Resource App
//
//  Created by Darryl Johnson on 4/16/14.
//  Copyright (c) 2014 Global App Initiative. All rights reserved.
//

#import "NavViewController.h"

@interface NavViewController ()

@end

@implementation NavViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    self.tabBarController.tabBar.backgroundImage = [UIImage imageNamed:@"color.png"];
    self.tabBarController.tabBar.tintColor = [UIColor whiteColor];
    for(UIViewController *tab in  self.tabBarController.viewControllers)
        
    {
        [tab.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                [UIFont fontWithName:@"Helvetica" size:15], UITextAttributeFont, nil]
                                      forState:UIControlStateNormal];
    }
    
    // Do any additional setup after loading the view.
}

-(void)viewWillLoad
{
    [super viewDidLoad];
}


-(void)viewDidDisappear:(BOOL)animated
{
     self.tabBarController.tabBar.backgroundImage = [UIImage imageNamed:@"color2.png"];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
