//
//  HotlineInfo.h
//  Teen Health Resource App
//
//  Created by Tom Strissel on 4/5/14.
//  Copyright (c) 2014 Global App Initiative. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HotlineInfo : NSObject{
    NSString *_hotlineId;
    NSString *_name;
}

@property (nonatomic, copy) NSString *hotlineId;
@property (nonatomic, copy) NSString *name;


- (id)init:(NSString*)uniqueId name:(NSString *)name;

@end
