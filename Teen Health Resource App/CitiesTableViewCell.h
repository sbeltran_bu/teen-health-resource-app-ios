//
//  CitiesTableViewCell.h
//  Teen Health Resource App
//
//  Created by Darryl Johnson on 4/16/14.
//  Copyright (c) 2014 Global App Initiative. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CitiesTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;

@end
