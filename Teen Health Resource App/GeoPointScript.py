import sqlite3 as lite
import re

addressDict = {}
                

def getData():
    #Connect to DB
    con = lite.connect('pheDatabase')

    cityID = 'Ce1fcfAtrj'
    tempAddress = ""
    
    with con:
        cur = con.cursor()    
        cur.execute("SELECT address FROM clinics WHERE city_id=?;", (cityID,))

    for row in cur:
        if(row == None):
            return False
        else:
            tempAddress = str(row)
            tempAddress = tempAddress.strip('(')
            tempAddress = tempAddress.strip(')')
            tempAddress = tempAddress.strip('\'')
            tempAddress = tempAddress.strip(',')
            tempAddress = tempAddress.strip('\'')
            cleanKey = tempAddress;
            #print(tempAddress)
            tempAddress = removeParen(tempAddress)
            #print(tempAddress)
            newAddress = tempAddress + ', Washington D.C.'
            #print(newAddress)
            addressDict[cleanKey] = newAddress
    con.close()
    print('DONE')

def removeParen(mystring):
    result = re.sub("[\(\[].*?[\)\]]", "", mystring)
    result = str(result)
    return result

def printDictionary():
    for keys,values in addressDict.items():
        print(keys)
        print(values)
    
def updateData():
    con = lite.connect('pheDatabase')

    for keys,values in addressDict.items():
        
        with con:
            cur = con.cursor()
            cur.execute("""UPDATE clinics SET address=? WHERE address=?;""", (values,keys))
            print('Updated -- Old: ' + keys + " New: " + values)
    
    con.close()

getData()
printDictionary()
updateData()
