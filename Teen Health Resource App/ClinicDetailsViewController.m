//
//  ClinicDetailsViewController.m
//  Teen Health Resource App
//
//  Created by Darryl Johnson on 4/15/14.
//  Copyright (c) 2014 Global App Initiative. All rights reserved.
//

#import "ClinicDetailsViewController.h"
#import "pheDatabase.h"

@interface ClinicDetailsViewController ()

@end

@implementation ClinicDetailsViewController

NSArray * clinics;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    
    self.MAP.delegate = self;
    
    self.viewToBeShadowed.layer.masksToBounds = NO;
    self.viewToBeShadowed.layer.shadowOffset = CGSizeMake(-.5, 0);
    self.viewToBeShadowed.layer.shadowRadius = 5;
    self.viewToBeShadowed.layer.shadowOpacity = 0.5;
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    NSLog(@"%@", [defaults objectForKey:@"CurrentClinic"]);
    
    clinics = [[PHEDatabase database] clinicForID:[defaults objectForKey:@"CurrentClinic"]];
   
    
    

    
    
    
if (![[[PHEDatabase database] clinicGeoPointsForclinic:[defaults objectForKey:@"CurrentClinic"] inCity:[defaults objectForKey:@"CurrentCity"]] isEqualToString:@""])
{
    NSArray * array = [[[PHEDatabase database] clinicGeoPointsForclinic:[defaults objectForKey:@"CurrentClinic"] inCity:[defaults objectForKey:@"CurrentCity"]] componentsSeparatedByString:@","];
    
    
    
    CLLocationCoordinate2D coord = {([array[0] doubleValue]),([array[1] doubleValue])};
    
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    point.coordinate = coord;
    [self.MAP addAnnotation:point];
    
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(coord, 2500, 2500);
    [self.MAP setRegion:[self.MAP regionThatFits:region] animated:YES];
    
    
}

   
    
    

    NSMutableArray * arrayOfImages = [[NSMutableArray alloc] initWithCapacity:20];
    self.navigationItem.title = clinics[0];
    self.clinicNameLabel.text = clinics[0];
    self.hoursText.text = clinics[2];
    self.AddressLabel.text = clinics[1];
    
    
    
    if([clinics[4] intValue] == 1)
    {
        [arrayOfImages addObject:@"lock.png"];
    }
    
    if([clinics[5] intValue] == 1)
    {
        [arrayOfImages addObject:@"dollar.png"];
    }
    else
    {
        [arrayOfImages addObject:@"bigDollar.png"];
    }
    
    if([clinics[6] intValue] == 1)
    {
        
        [arrayOfImages addObject:@"baby.png"];
    }
    
    
    @try {
        self.img1.image = [UIImage imageNamed:arrayOfImages[0]];
    }
    @catch (NSException *exception) {
        self.img1.hidden = YES;
    }
    
    @try {
        self.img2.image = [UIImage imageNamed:arrayOfImages[1]];
    }
    @catch (NSException *exception) {
        self.img2.hidden = YES;
    }
    
    @try {
        self.img3.image = [UIImage imageNamed:arrayOfImages[2]];
    }
    @catch (NSException *exception) {
        self.img3.hidden = YES;
    }
    
    
   
    
    self.walkInYes.text = [[PHEDatabase database] extraDetailsForClinic:[defaults objectForKey:@"CurrentClinic"] inCity:[defaults objectForKey:@"CurrentCity"]];
    
      
        
    
    
  [self.callButton setTitle:[NSString stringWithFormat:@"Call: %@", clinics[3]] forState:UIControlStateNormal];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)callButtonPressed:(id)sender {
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@", clinics[3]]]];
    
}


- (IBAction)directionButtonPresses:(id)sender {
    
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    NSArray * array = [[[PHEDatabase database] clinicGeoPointsForclinic:[defaults objectForKey:@"CurrentClinic"] inCity:[defaults objectForKey:@"CurrentCity"]] componentsSeparatedByString:@","];
    CLLocationCoordinate2D coord = {([array[0] doubleValue]),([array[1] doubleValue])};
    MKPlacemark* place = [[MKPlacemark alloc] initWithCoordinate: coord addressDictionary: nil];
    MKMapItem* destination = [[MKMapItem alloc] initWithPlacemark: place];
    destination.name = clinics[0];
    NSArray* items = [[NSArray alloc] initWithObjects: destination, nil];
    NSDictionary* options = [[NSDictionary alloc] initWithObjectsAndKeys:
                             MKLaunchOptionsDirectionsModeDriving,
                             MKLaunchOptionsDirectionsModeKey, nil];
    [MKMapItem openMapsWithItems: items launchOptions: options];
    
    
    
    
    
    
}


@end
