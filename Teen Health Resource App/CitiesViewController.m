//
//  CitiesViewController.m
//  Teen Health Resource App
//
//  Created by Darryl Johnson on 3/26/14.
//  Copyright (c) 2014 Global App Initiative. All rights reserved.
//

#import "CitiesViewController.h"
#import "CitiesTableViewCell.h"
#import "AppDelegate.h"
#import "pheDatabase.h"
@interface CitiesViewController ()

@end

@implementation CitiesViewController
@synthesize Cities = _Cities;
NSMutableArray * cities;
NSString *currentTitle;

@synthesize citiesTable = _citiesTable;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (void)viewDidLoad
{
    currentTitle = @"";
    cities = [[NSMutableArray alloc] initWithArray:[[PHEDatabase database] cityList]];
    [[PHEDatabase database] printAllGeos];
    
    self.citiesTable.delegate = self;
    self.citiesTable.dataSource = self;
    self.citiesTable.separatorColor = [UIColor blackColor];
    self.navigationItem.title = @"Locations";
    
    [super viewDidLoad];
    

    // Do any additional setup after loading the view.
};







- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    //i made a change
    
    return [cities count]/2;
    
}




-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"City"];
    
    
    
    ((CitiesTableViewCell *)cell).titleLabel.text = [cities objectAtIndex:indexPath.item*2];

   
    
    
return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    

    
    
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[cities objectAtIndex:indexPath.item*2+1] forKey:@"CurrentCity"];
    currentTitle = [cities objectAtIndex:indexPath.item*2];
    [self performSegueWithIdentifier:@"proceedToCity" sender:self];
    
    
    
    
    
}



 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
     NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
     ((UIViewController *)[segue destinationViewController]).navigationItem.title = currentTitle;
     [defaults setObject:currentTitle forKey:@"CurrentCityTitle"];
 }






@end
