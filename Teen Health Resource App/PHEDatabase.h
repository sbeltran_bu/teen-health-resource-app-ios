//
//  PHEDatabase.h
//  Teen Health Resource App
//
//  Created by Tom Strissel on 4/5/14.
//  Copyright (c) 2014 Global App Initiative. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
@class HotlineDetails;

@interface PHEDatabase : NSObject{
    sqlite3 *_database;
}

+ (PHEDatabase*)database;
- (NSArray *)hotlineCategoryInfos;
- (NSArray *)cityList;
- (NSArray *)clinicsWhereCityIDis:(NSString *)city;
- (NSArray *)clinicsNamesWhereCityis:(NSString *)city;
- (HotlineDetails *)hotlineDetails:(NSString*)hotlineId cityId:(NSString*)cityId;

- (NSArray *)clinicsiconsWhereCityNameIs:(NSString *)city;
- (NSArray *)clinicForID:(NSString *)clinic;
- (NSString *)clinicGeoPointsForclinic:(NSString *)clinic inCity:(NSString *)city;
- (void)printAllGeos;
- (NSString *)extraDetailsForClinic:(NSString *)clinic inCity:(NSString *)city;
- (NSArray *)geoPointsforcity:(NSString *)city;
- (NSArray *)clinicsAddressWhereCityis:(NSString *)city;
@end
