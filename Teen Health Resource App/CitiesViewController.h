//
//  CitiesViewController.h
//  Teen Health Resource App
//
//  Created by Darryl Johnson on 3/26/14.
//  Copyright (c) 2014 Global App Initiative. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CitiesViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *citiesTable;
@property (nonatomic, strong) NSMutableDictionary * Cities;
@end
