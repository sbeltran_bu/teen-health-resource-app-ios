//
//  ViewController.h
//  Teen Health Resource App
//
//  Created by Santiago Beltran on 3/19/14.
//  Copyright (c) 2014 Global App Initiative. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>


@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end
