//
//  PHEDatabase.m
//  Teen Health Resource App
//
//  Created by Tom Strissel on 4/5/14.
//  Copyright (c) 2014 Global App Initiative. All rights reserved.
//

#import "PHEDatabase.h"
#import "HotlineInfo.h"

@implementation PHEDatabase
static PHEDatabase *_database;

+ (PHEDatabase*)database {
    if (_database == nil) {
        _database = [[PHEDatabase alloc] init];
    }
    return _database;
}

- (id)init {
    if ((self = [super init])) {
        NSString *sqLiteDb = [[NSBundle mainBundle] pathForResource:@"pheDatabase"
                                                             ofType:@""];
        
        if (sqlite3_open([sqLiteDb UTF8String], &_database) != SQLITE_OK) {
            NSLog(@"Failed to open database!");
        }
 
    }
    return self;
}

- (void)dealloc {
    sqlite3_close(_database);
    //[super dealloc];
}

- (NSArray *)hotlineCategoryInfos {
    
    NSMutableArray *retval = [[NSMutableArray alloc] init];
    NSString *query = @"SELECT * FROM categories";
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, nil)
        == SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            char *uniqueIdChars = (char *) sqlite3_column_text(statement, 1);
            char *nameChars = (char *) sqlite3_column_text(statement, 2);
            NSString *hotlineId = [[NSString alloc] initWithUTF8String:uniqueIdChars];
            NSString *name = [[NSString alloc] initWithUTF8String:nameChars];
            HotlineInfo *info = [[HotlineInfo alloc]
                                    init:hotlineId name:name];
            [retval addObject:info];
        }
        sqlite3_finalize(statement);
    }
    else {
        NSLog(@"Error while inserting data. '%s'", sqlite3_errmsg(_database));
    }
    return retval;
    
}

- (NSMutableArray *)hotlineDetails:(NSString*)hotlineId cityId:(NSString*)cityId {
    NSMutableArray *retval = [[NSMutableArray alloc] init];
    NSString *query = [NSString stringWithFormat:@"SELECT * FROM hotlines WHERE hotlines.hotline_id= '%@' and hotlines.city_id='%@'", hotlineId, cityId];
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, nil)== SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            char *nameChars = (char *) sqlite3_column_text(statement, 4);
            char *phoneChars = (char *) sqlite3_column_text(statement, 5);
            char *extraDetailsChar = (char *) sqlite3_column_text(statement, 6);
    
            NSString *name = [[NSString alloc] initWithUTF8String:nameChars];
            NSString *phone = [[NSString alloc] initWithUTF8String:phoneChars];
            NSString *extraDetail;
            if (extraDetailsChar == NULL)
            {
            extraDetail = @"";
            }
            else{
                extraDetail = [[NSString alloc] initWithUTF8String:extraDetailsChar];
            }
            
            NSMutableArray *details = [[NSMutableArray alloc] init];
            [details addObject:name];
            [details addObject:phone];
            [details addObject:extraDetail];
        
            [retval addObject:details];
        }
        
        sqlite3_finalize(statement);

    }
    else {
        NSLog(@"Error while inserting data. '%s'", sqlite3_errmsg(_database));
    }
    
    return retval;
}
                   
- (NSArray *)cityList {
    
    NSMutableArray *retval = [[NSMutableArray alloc] init];
    NSString *query = @"SELECT * FROM cities";
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, nil)
        == SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            char *ID = (char *) sqlite3_column_text(statement, 1);
            char *nameChars = (char *) sqlite3_column_text(statement, 2);
            NSString *name = [[NSString alloc] initWithUTF8String:nameChars];
            NSString *nameID = [[NSString alloc] initWithUTF8String:ID];
        
            [retval addObject:name];
            [retval addObject:nameID];
        }
        sqlite3_finalize(statement);
    }
    else {
        NSLog(@"Error while inserting data. '%s'", sqlite3_errmsg(_database));
    }
    return retval;
    
}


- (NSArray *)clinicsWhereCityIDis:(NSString *)city {
    
    NSMutableArray *retval = [[NSMutableArray alloc] init];
    
    NSString * query = [NSString stringWithFormat:@"SELECT * FROM clinics WHERE city_id='%@'", city];
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, nil)
        == SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            char *ID = (char *) sqlite3_column_text(statement, 1);
            char *nameChars = (char *) sqlite3_column_text(statement, 3);
            char *addressChars = (char *) sqlite3_column_text(statement, 4);
            char *hoursChar = (char *) sqlite3_column_text(statement, 5);
            char *phoneChar = (char *) sqlite3_column_text(statement, 6);
            NSString *clinicID = [[NSString alloc] initWithUTF8String:ID];
            NSString *name = [[NSString alloc] initWithUTF8String:nameChars];
            NSString *address = [[NSString alloc] initWithUTF8String:addressChars];
            NSString *hours = [[NSString alloc] initWithUTF8String:hoursChar];
            NSString *phone = [[NSString alloc] initWithUTF8String:phoneChar];
            [retval addObject:clinicID];
            [retval addObject:name];
            [retval addObject:address];
            [retval addObject:hours];
            [retval addObject:phone];
        }
        sqlite3_finalize(statement);
    }
    else {
        NSLog(@"Error while inserting data. '%s'", sqlite3_errmsg(_database));
    }
    return retval;
    
}



- (NSArray *)clinicsNamesWhereCityis:(NSString *)city {
    
    NSMutableArray *retval = [[NSMutableArray alloc] init];
    
    NSString * query = [NSString stringWithFormat:@"SELECT * FROM clinics WHERE city_id='%@'", city];
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, nil)
        == SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
          
            char *nameChars = (char *) sqlite3_column_text(statement, 3);

            NSString *name = [[NSString alloc] initWithUTF8String:nameChars];

   
            [retval addObject:name];
      
        }
        sqlite3_finalize(statement);
    }
    else {
        NSLog(@"Error while inserting data. '%s'", sqlite3_errmsg(_database));
    }
    return retval;
    
}

- (NSArray *)clinicsAddressWhereCityis:(NSString *)city {
    
    NSMutableArray *retval = [[NSMutableArray alloc] init];
    
    NSString * query = [NSString stringWithFormat:@"SELECT * FROM clinics WHERE city_id='%@'", city];
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, nil)
        == SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            
            char *nameChars = (char *) sqlite3_column_text(statement, 4);
            
            NSString *name = [[NSString alloc] initWithUTF8String:nameChars];
            
            
            [retval addObject:name];
            
        }
        sqlite3_finalize(statement);
    }
    else {
        NSLog(@"Error while inserting data. '%s'", sqlite3_errmsg(_database));
    }
    return retval;
    
}


- (NSArray *)clinicsiconsWhereCityNameIs:(NSString *)city {
    
    NSMutableArray *retval = [[NSMutableArray alloc] init];
    
    NSString * query = [NSString stringWithFormat:@"SELECT * FROM clinics WHERE city_id='%@'", city];
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, nil)
        == SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            
            char *confidentialChars = (char *) sqlite3_column_text(statement, 8);
            char *lowcostChars = (char *) sqlite3_column_text(statement, 9);
            char *reproductiveChars = (char *) sqlite3_column_text(statement, 10);
            NSString *confidential = [[NSString alloc] initWithUTF8String:confidentialChars];
            NSString *lowCost = [[NSString alloc] initWithUTF8String:lowcostChars];
            NSString *reproductive = [[NSString alloc] initWithUTF8String:reproductiveChars];
            
            
            [retval addObject:confidential];
            [retval addObject:lowCost];
            [retval addObject:reproductive];
            
        }
        sqlite3_finalize(statement);
    }
    else {
        NSLog(@"Error while inserting data. '%s'", sqlite3_errmsg(_database));
    }
    return retval;
    
}




- (NSArray *)clinicForID:(NSString *)clinic {
    
    NSMutableArray *retval = [[NSMutableArray alloc] init];
    
    NSString * query = [NSString stringWithFormat:@"SELECT * FROM clinics WHERE id='%@'", clinic];
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, nil)
        == SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            
            char *nameChars = (char *) sqlite3_column_text(statement, 3);
            char *addressChars = (char *) sqlite3_column_text(statement, 4);
            char *hoursChars = (char *) sqlite3_column_text(statement, 5);
            char *phoneChars = (char *) sqlite3_column_text(statement, 6);
            char *confidentialChars = (char *) sqlite3_column_text(statement, 8);
            char *lowCostChars = (char *) sqlite3_column_text(statement, 9);
            char *reproductiveChars = (char *) sqlite3_column_text(statement, 10);
            
            NSString *name = [[NSString alloc] initWithUTF8String:nameChars];
            NSString *address = [[NSString alloc] initWithUTF8String:addressChars];
            NSString *hours = [[NSString alloc] initWithUTF8String:hoursChars];
            NSString *phone = [[NSString alloc] initWithUTF8String:phoneChars];
            NSString *confidential = [[NSString alloc] initWithUTF8String:confidentialChars];
            NSString *lowCost = [[NSString alloc] initWithUTF8String:lowCostChars];
            NSString *reproductive = [[NSString alloc] initWithUTF8String:reproductiveChars];
            
            
            [retval addObject:name];
            [retval addObject:address];
            [retval addObject:hours];
            [retval addObject:phone];
            [retval addObject:confidential];
            [retval addObject:lowCost];
            [retval addObject:reproductive];
            
        }
        sqlite3_finalize(statement);
    }
    else {
        NSLog(@"Error while inserting data. '%s'", sqlite3_errmsg(_database));
    }
    return retval;
    
}



- (NSString *)clinicGeoPointsForclinic:(NSString *)clinic inCity:(NSString *)city {


   
    
    NSString * query = [NSString stringWithFormat:@"SELECT * FROM clinics WHERE id=\'%@\' AND city_id=\'%@\'", clinic, city];
    NSLog(@"%@", query);
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, nil)
        == SQLITE_OK) {
        
        
        while (sqlite3_step(statement) == SQLITE_ROW) {
            
            
            
            
            
                char *nameChars = (char *) sqlite3_column_text(statement, 11);

                if (nameChars)
                    return [NSString stringWithUTF8String:(const char *)nameChars];
            
                else
                    return [NSString stringWithFormat:@""];
            
            
            
      
         
            
        }
        sqlite3_finalize(statement);
    }
    else {
        NSLog(@"Error while inserting data. '%s'", sqlite3_errmsg(_database));
    }
    return nil;
    
}

- (NSString *)extraDetailsForClinic:(NSString *)clinic inCity:(NSString *)city {
    
    
    
    
    NSString * query = [NSString stringWithFormat:@"SELECT * FROM clinics WHERE id=\'%@\' AND city_id=\'%@\'", clinic, city];
    NSLog(@"%@", query);
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, nil)
        == SQLITE_OK) {
        
        
        while (sqlite3_step(statement) == SQLITE_ROW) {
            
            
            
            
            
            char *nameChars = (char *) sqlite3_column_text(statement, 7);
            
            if (nameChars)
                return [NSString stringWithUTF8String:(const char *)nameChars];
            
            else
                return [NSString stringWithFormat:@""];
            
            
            
            
            
            
        }
        sqlite3_finalize(statement);
    }
    else {
        NSLog(@"Error while inserting data. '%s'", sqlite3_errmsg(_database));
    }
    return nil;
    
}



- (NSArray *)geoPointsforcity:(NSString *)city {
    
    
    
     NSMutableArray *retval = [[NSMutableArray alloc] init];
    NSString * query = [NSString stringWithFormat:@"SELECT * FROM clinics WHERE city_id=\'%@\'", city];
    NSLog(@"%@", query);
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, nil)
        == SQLITE_OK) {
        
        
        while (sqlite3_step(statement) == SQLITE_ROW) {
            
            
            
            
            
            char *nameChars = (char *) sqlite3_column_text(statement, 11);
            
            if (nameChars)
                [retval addObject:[NSString stringWithUTF8String:(const char *)nameChars]];
            
            else
                [retval addObject:[NSString stringWithFormat:@""]];
            
            
            
            
            
            
        }
        sqlite3_finalize(statement);
    }
    else {
        NSLog(@"Error while inserting data. '%s'", sqlite3_errmsg(_database));
    }
    return retval;
    
}


- (void)printAllGeos
{
    

    return;
}


@end
