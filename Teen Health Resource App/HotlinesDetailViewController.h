//
//  HotlinesDetailViewController.h
//  Teen Health Resource App
//
//  Created by Tom Strissel on 4/14/14.
//  Copyright (c) 2014 Global App Initiative. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HotlinesDetailViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>


@property (retain, nonatomic) NSMutableArray *details;


@property (nonatomic, copy) NSString *hotlineId;



@end
