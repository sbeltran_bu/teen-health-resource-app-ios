//
//  ClinicsTableViewCell.m
//  Teen Health Resource App
//
//  Created by Darryl Johnson on 4/14/14.
//  Copyright (c) 2014 Global App Initiative. All rights reserved.
//

#import "ClinicsTableViewCell.h"

@implementation ClinicsTableViewCell
@synthesize Title = _Title;
@synthesize image1 = _image1;
@synthesize image2 = _image2;
@synthesize image3 = _image3;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
