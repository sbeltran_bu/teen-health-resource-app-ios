//
//  MapViewController.h
//  Teen Health Resource App
//
//  Created by Darryl Johnson on 4/9/14.
//  Copyright (c) 2014 Global App Initiative. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

@interface MapViewController : UIViewController <MKMapViewDelegate, UIAlertViewDelegate>
@property (strong, nonatomic) IBOutlet MKMapView *MAP;


@end
