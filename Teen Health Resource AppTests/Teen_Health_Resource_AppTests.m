//
//  Teen_Health_Resource_AppTests.m
//  Teen Health Resource AppTests
//
//  Created by Santiago Beltran on 3/19/14.
//  Copyright (c) 2014 Global App Initiative. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface Teen_Health_Resource_AppTests : XCTestCase

@end

@implementation Teen_Health_Resource_AppTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end
